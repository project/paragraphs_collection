<?php

namespace Drupal\Tests\paragraphs_collection\Functional;

use Drupal\Tests\paragraphs\Functional\WidgetStable\ParagraphsTestBase;

/**
 * Tests configuration handling within Paragraphs Collection.
 *
 * @group paragraphs_collection
 * @requires module paragraphs
 */
class ParagraphsCollectionConfigurationTest extends ParagraphsTestBase {

  /**
   * Tests if reinstalling module delete configuration files.
   */
  public function testReinstallingModule(){
    \Drupal::service('module_installer')->install(['paragraphs_collection']);
    \Drupal::service('module_installer')->uninstall(['paragraphs_collection']);
    \Drupal::service('module_installer')->install(['paragraphs_collection']);
  }

}
