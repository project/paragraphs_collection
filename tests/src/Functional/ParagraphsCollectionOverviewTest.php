<?php

namespace Drupal\Tests\paragraphs_collection\Functional;

use Drupal\paragraphs\Entity\ParagraphsType;
use Drupal\Tests\paragraphs\Functional\WidgetStable\ParagraphsTestBase;

/**
 * Tests the Paragraphs Collection overview pages.
 *
 * @group paragraphs_collection
 */
class ParagraphsCollectionOverviewTest extends ParagraphsTestBase {

  /**
   * Modules to be enabled.
   *
   * @var array
   */
  protected static $modules = [
    'paragraphs_collection',
    'paragraphs_collection_demo',
    'paragraphs_collection_test',
  ];

  /**
   * {@inheritdoc}
   */
  protected function setUp(): void {
    parent::setUp();
  }

  /**
   * Tests the overview pages of discoverable items.
   *
   * These are the layouts for the grid layout plugin and styles for the style
   * plugin.
   */
  public function testOverviewPages() {
    $this->loginAsAdmin([
      'administer paragraphs types',
      'access site reports',
    ]);

    // Check the new link on the reports page.
    $this->drupalGet('/admin/reports');
    $this->assertSession()->pageTextContains('Overviews of items discoverable by behavior plugins.');
    $this->clickLink('Paragraphs Collection');

    // Check the grid layouts overview.
    $this->assertSession()->addressEquals('/admin/reports/paragraphs_collection/layouts');
    $this->assertSession()->titleEquals('Available grid layouts | Drupal');
    $this->assertSession()->pageTextContains('Grid layout label or ID');
    $this->assertSession()->pageTextContains('Details');
    $this->assertSession()->pageTextContains('Used in');

    // Check that a concrete grid layout is displayed.
    $this->assertSession()->pageTextContains('Three columns 1 - 1 - 2');
    $this->assertSession()->pageTextContains('Three columns layout of 1/4, 1/4 and 1/2 width.');
    $this->assertSession()->pageTextContains('paragraphs_collection_demo_1_1_2_column');
    $this->assertSession()->linkExists('Grid');
    $this->assertSession()->linkByHrefExists('/admin/structure/paragraphs_type/grid');

    // Check the tabs.
    $this->assertSession()->linkExists('Layouts');
    $this->clickLink('Styles');

    // Check the styles layouts overview.
    $this->assertSession()->addressEquals('/admin/reports/paragraphs_collection/styles');
    $this->assertSession()->titleEquals('Available styles | Drupal');
    $this->assertSession()->pageTextContains('Group');
    $this->assertSession()->pageTextContains('Style label or ID');
    $this->assertSession()->pageTextContains('Details');
    $this->assertSession()->pageTextContains('Used in');

    // Check that a concrete style is displayed.
    $this->assertSession()->pageTextContains('Blue');
    $this->assertSession()->pageTextContains('paragraphs-blue');
    $this->assertSession()->pageTextContains('General Group');
    $this->assertSession()->linkExists('Container');
    $this->assertSession()->linkByHrefExists('/admin/structure/paragraphs_type/container');

    // Check the tabs.
    $this->assertSession()->linkExists('Layouts');
    $this->assertSession()->linkExists('Styles');

    // Disable the grid layout and style plugins for all paragraphs types.
    $paragraph_type_ids = \Drupal::entityQuery('paragraphs_type')->execute();
    $paragraphs_types = ParagraphsType::loadMultiple($paragraph_type_ids);
    foreach ($paragraphs_types as $paragraphs_type) {
      /** @var \Drupal\paragraphs\ParagraphsTypeInterface $paragraphs_type */
      $paragraphs_type->getBehaviorPlugin('grid_layout')->setConfiguration(['enabled' => FALSE]);
      $paragraphs_type->getBehaviorPlugin('style')->setConfiguration(['enabled' => FALSE]);
      $paragraphs_type->save();
    }

    // Check the grid layouts overview page displays grid layouts but no
    // paragraphs types.
    $this->drupalGet('/admin/reports/paragraphs_collection/layouts');
    $this->assertSession()->pageTextContains('Three columns 1 - 1 - 2');
    $this->assertSession()->pageTextContains('Three columns layout of 1/4, 1/4 and 1/2 width.');
    $this->assertSession()->pageTextContains('paragraphs_collection_demo_1_1_2_column');
    $this->assertSession()->linkNotExists('Grid');
    $this->assertSession()->linkByHrefNotExists('/admin/structure/paragraphs_type/grid');

    // Check the styles overview page displays styles but no paragraphs types.
    $this->drupalGet('/admin/reports/paragraphs_collection/styles');
    $this->assertSession()->pageTextContains('Blue');
    $this->assertSession()->pageTextContains('paragraphs-blue');
    $this->assertSession()->pageTextContains('General Group');
    $this->assertSession()->linkNotExists('Container');
    $this->assertSession()->linkByHrefNotExists('/admin/structure/paragraphs_type/container');

    // Assert saved styles are sorted alphabetically.
    $this->drupalGet('/admin/reports/paragraphs_collection/styles');
    $this->submitForm([
      'styles[underline][enabled]' => TRUE,
      'styles[advanced][enabled]' => TRUE,
    ], 'Save configuration');
    $this->assertSession()->pageTextContains('The configuration options have been saved.');
    $this->assertEquals([
      'advanced',
      'underline',
    ], \Drupal::config('paragraphs_collection.settings')->get('enabled_styles'));
  }

}
