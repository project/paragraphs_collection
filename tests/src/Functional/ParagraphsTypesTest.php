<?php

namespace Drupal\Tests\paragraphs_collection\Functional;

use Drupal\Tests\paragraphs\Functional\WidgetStable\ParagraphsTestBase;

/**
 * Tests the Paragraphs Collection paragraph types.
 *
 * @group paragraphs_collection
 * @requires module paragraphs
 */
class ParagraphsTypesTest extends ParagraphsTestBase {

  /**
   * Modules to be enabled.
   */
  protected static $modules = [
    'paragraphs_collection',
    'paragraphs_library',
  ];

  /**
   * Tests adding the existing paragraph types.
   */
  public function testAddParagraphTypes() {
    $this->addParagraphedContentType('paragraphed_test');
    $this->loginAsAdmin([
      'create paragraphed_test content',
      'edit any paragraphed_test content',
      'administer paragraphs library',
    ]);
    $this->drupalGet('/node/add/paragraphed_test');
    $this->submitForm([], 'field_paragraphs_intro_add_more');
    $this->submitForm([], 'field_paragraphs_quote_add_more');
    $this->submitForm([], 'field_paragraphs_separator_add_more');
    $this->submitForm([], 'field_paragraphs_subtitle_add_more');
    $this->submitForm([], 'field_paragraphs_title_add_more');
    $this->submitForm([], 'field_paragraphs_user_add_more');
    $this->submitForm([], 'field_paragraphs_footer_add_more');

    $edit = [
      'title[0][value]' => 'Paragraph types example',
      'field_paragraphs[0][subform][paragraphs_text][0][value]' => 'Intro test',
      'field_paragraphs[1][subform][paragraphs_quote_text][0][value]' => 'Quote test',
      'field_paragraphs[1][subform][paragraphs_quote_author][0][value]' => 'Author test',
      'field_paragraphs[3][subform][paragraphs_subtitle][0][value]' => 'Subtitle test',
      'field_paragraphs[4][subform][paragraphs_title][0][value]' => 'Title test',
      'field_paragraphs[5][subform][paragraphs_user][0][target_id]' => $this->admin_user->getAccountName() . ' (' . $this->admin_user->id() . ')',
      'field_paragraphs[6][subform][paragraphs_text][0][value]' => 'Footer test',
    ];

    $this->submitForm($edit, 'Save');

    // Checks content.
    $this->assertSession()->pageTextContains('Intro test');
    $this->assertSession()->pageTextContains($this->admin_user->getDisplayName());
    $this->assertSession()->pageTextContains('Footer test');
    $this->assertSession()->pageTextContains('Subtitle test');
    $this->assertSession()->pageTextContains('Title test');

    // Asserts the quote paragraph type.
    $elements = $this->xpath('//blockquote[contains(@class, class)]', [':class' => 'paragraph--type--quote']);
    $this->assertCount(1, $elements);
    $this->assertSession()->pageTextContains('Quote test');
    $this->assertSession()->pageTextContains('Author test');

    // Adds the link paragraph type.
    $this->drupalGet('/node/add/paragraphed_test');
    $this->submitForm([], 'field_paragraphs_link_add_more');
    $edit = [
      'title[0][value]' => 'Link example',
      'field_paragraphs[0][subform][paragraphs_link][0][uri]' => 'Paragraph types example (1)',
      'field_paragraphs[0][subform][paragraphs_link][0][title]' => 'Link test',
    ];
    $this->submitForm($edit, 'Save');

    // Checks if the link type is working properly.
    $this->clickLink('Link test');
    $this->assertSession()->pageTextContains('Paragraph types example');

  }

  /**
   * Ensures that a new paragraph type is created.
   */
  public function testCreateParagraphType() {
    $this->loginAsAdmin();
    $this->drupalGet('/admin/structure/paragraphs_type');
    $this->clickLink(t('Add paragraph type'));
    $edit = [
      'label' => 'test_paragraph',
      'id' => 'test_paragraph',
    ];
    $this->submitForm($edit, t('Save and manage fields'));
    $this->assertSession()->pageTextContains('Saved the test_paragraph Paragraphs type');
  }

}
