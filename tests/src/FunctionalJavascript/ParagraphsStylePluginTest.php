<?php

namespace Drupal\Tests\paragraphs_collection\FunctionalJavascript;

use Drupal\Core\StringTranslation\TranslatableMarkup;
use Drupal\FunctionalJavascriptTests\WebDriverTestBase;
use Drupal\node\Entity\Node;
use Drupal\paragraphs\Entity\Paragraph;
use Drupal\Tests\field_ui\Traits\FieldUiTestTrait;
use Drupal\Tests\paragraphs\FunctionalJavascript\LoginAdminTrait;
use Drupal\Tests\paragraphs\FunctionalJavascript\ParagraphsTestBaseTrait;

/**
 * Tests the style selection plugin.
 *
 * @see \Drupal\paragraphs_collection\Plugin\paragraphs\Behavior\ParagraphsStylePlugin
 * @group paragraphs_collection
 */
class ParagraphsStylePluginTest extends WebDriverTestBase {

  use LoginAdminTrait;
  use ParagraphsTestBaseTrait;
  use FieldUiTestTrait;

  /**
   * Modules to be enabled.
   */
  protected static $modules = [
    'node',
    'paragraphs',
    'field',
    'field_ui',
    'block',
    'paragraphs_test',
    'paragraphs_collection',
  ];

  /**
   * {@inheritdoc}
   */
  protected $defaultTheme = 'stark';

  /**
   * {@inheritdoc}
   */
  protected function setUp(): void {
    parent::setUp();

    // Place the breadcrumb, tested in fieldUIAddNewField().
    $this->drupalPlaceBlock('system_breadcrumb_block');
    $this->drupalPlaceBlock('local_tasks_block');
    $this->drupalPlaceBlock('local_actions_block');
    $this->drupalPlaceBlock('page_title_block');
  }

  /**
   * Tests the advanced style functionality.
   */
  public function testAdvancedStyles() {
    // Install Paragraph Collection Test in order to have styles.
    \Drupal::service('module_installer')->install(['paragraphs_collection_test']);

    $this->addParagraphedContentType('paragraphed_test', 'paragraphs');
    $this->loginAsAdmin([
      'edit any paragraphed_test content',
      'edit behavior plugin settings',
      'use advanced style',
    ]);
    $this->drupalGet('admin/structure/paragraphs_type/add');

    // Create Paragraph type with Style plugin enabled.
    $paragraph_type = 'test_style_plugin';
    $this->addParagraphsType($paragraph_type);
    // Add a text field.
    $this->fieldUIAddExistingField('admin/structure/paragraphs_type/' . $paragraph_type, 'paragraphs_text', $paragraph_type);

    $this->drupalGet('admin/structure/paragraphs_type/' . $paragraph_type);
    $this->click('#edit-behavior-plugins-style-enabled');
    $this->click('#edit-behavior-plugins-style-settings-groups-advanced-test-group');
    $this->assertSession()->assertWaitOnAjaxRequest();

    $edit = [
      'behavior_plugins[style][settings][groups_defaults][advanced_test_group][default]' => '',
    ];
    $this->submitForm($edit, t('Save'));

    // Create paragraphed content.
    $this->drupalGet('node/add/paragraphed_test');
    $page = $this->getSession()->getPage();
    $page->pressButton('List additional actions');
    $page->pressButton('paragraphs_test_style_plugin_add_more');
    $this->assertSession()->assertWaitOnAjaxRequest();

    // Assert a user has no access to super advanced style.
    $options = $this->xpath('//select[contains(@id, :id)]/option', [':id' => 'edit-paragraphs-0-behavior-plugins-style-style']);
    $this->assertCount(2, $options);
    $this->assertEquals('- Default -', $options[0]->getHtml());
    $this->assertEquals('Advanced', $options[1]->getHtml());

    // Apply advanced style.
    $page->fillField('title[0][value]', 'advanced_style');
    $page->fillField('paragraphs[0][subform][paragraphs_text][0][value]', 'I am text enhanced with advanced style.');
    $page->clickLink('Behavior');
    $page->fillField('paragraphs[0][behavior_plugins][style][style_wrapper][styles][advanced_test_group]', 'advanced');
    $page->pressButton('Save');
    // Advanced style has been applied.
    $this->assertCount(1, $this->cssSelect('.paragraphs-behavior-style--advanced'));
    // Assert that the attributes are visible.
    $this->assertCount(1, $this->cssSelect('[data-attribute="test"]'));

    // Set advanced style as a default one.
    $this->drupalGet('admin/structure/paragraphs_type/' . $paragraph_type);
    $edit = [
      'behavior_plugins[style][settings][groups][advanced_test_group]' => TRUE,
      'behavior_plugins[style][settings][groups_defaults][advanced_test_group][default]' => 'advanced',
    ];
    $this->submitForm($edit, t('Save'));

    // Anonymous users still see the advanced style applied.
    $node = $this->getNodeByTitle('advanced_style');
    $this->drupalLogout();
    $this->drupalGet($node->toUrl());
    $this->assertCount(1, $this->cssSelect('.paragraphs-behavior-style--advanced'));

    // Advanced style can not be changed without the style permission.
    $this->loginAsAdmin([
      'edit any paragraphed_test content',
      'edit behavior plugin settings',
      'use super-advanced style',
    ]);
    $this->drupalGet($node->toUrl('edit-form'));

    // User cannot update the advanced style.
    $styles = $this->xpath('//select[@name="paragraphs[0][behavior_plugins][style][style_wrapper][styles][advanced_test_group]"]');
    $this->assertEquals('disabled', $styles[0]->getAttribute('disabled'));

    // As the user can not access advanced style and as with super-advanced
    // style there would be only element in the list, no style selection is
    // displayed.
    $page->pressButton('List additional actions');
    $page->pressButton('paragraphs_test_style_plugin_add_more');
    $this->assertSession()->assertWaitOnAjaxRequest();
    $styles = $this->xpath('//select[@name="paragraphs[1][behavior_plugins][style][style_wrapper][styles][advanced_test_group]"]');
    $this->assertSession()->elementNotExists('css', 'select[name="paragraphs[1][behavior_plugins][style][style_wrapper][styles][advanced_test_group]"]');
    $this->submitForm([], 'Save');
    // The advanced (default) style was applied to the second text paragraph.
    $this->assertCount(2, $this->cssSelect('.paragraphs-behavior-style--advanced'));
  }

  /**
   * Tests the style selection plugin settings and functionality.
   */
  public function testStyleSelection() {
    // Install Paragraph Collection Test in order to have styles.
    \Drupal::service('module_installer')->install(['paragraphs_collection_test']);

    $this->addParagraphedContentType('paragraphed_test', 'paragraphs');
    $this->loginAsAdmin([
      'edit any paragraphed_test content',
      'edit behavior plugin settings',
    ]);
    $this->drupalGet('admin/structure/paragraphs_type/add');

    // Create Paragraph type with Style plugin enabled.
    $paragraph_type = 'test_style_plugin';
    $this->addParagraphsType($paragraph_type);
    // Add a text field.
    $this->fieldUIAddExistingField('admin/structure/paragraphs_type/' . $paragraph_type, 'paragraphs_text', $paragraph_type);
    $this->drupalGet('admin/structure/paragraphs_type/' . $paragraph_type);
    $edit = [
      'behavior_plugins[style][enabled]' => TRUE,
    ];
    $this->submitForm($edit, t('Save'));
    $this->assertSession()->pageTextContains('The style plugin cannot be enabled if no groups are selected.');
    $this->click('#edit-behavior-plugins-style-settings-groups-regular-test-group');
    $this->assertSession()->assertWaitOnAjaxRequest();
    $edit = [
      'behavior_plugins[style][settings][groups_defaults][regular_test_group][default]' => '',
    ];
    $this->submitForm($edit, t('Save'));

    // Create paragraphed content.
    $this->drupalGet('node/add/paragraphed_test');
    $page = $this->getSession()->getPage();
    $page->pressButton('List additional actions');
    $page->pressButton('paragraphs_test_style_plugin_add_more');
    $this->assertSession()->assertWaitOnAjaxRequest();

    // Check that we have style plugin.
    $this->assertSession()->pageTextContains('style');
    $this->assertSession()->fieldExists('paragraphs[0][behavior_plugins][style][style_wrapper][styles][regular_test_group]');

    // Check that the style options are sorted alphabetically.
    $options = $this->xpath('//select[contains(@id, :id)]/option', [':id' => 'edit-paragraphs-0-behavior-plugins-style-style']);
    $this->assertEquals('- Default -', $options[0]->getHtml());
    $this->assertEquals('Bold', $options[1]->getHtml());
    $this->assertEquals('Overridden style Module', $options[2]->getHtml());
    $this->assertEquals('Regular', $options[3]->getHtml());

    // Restrict the paragraphs type to the "Italic Test Group" style group.
    $this->drupalGet('admin/structure/paragraphs_type/' . $paragraph_type);
    $this->assertSession()->fieldExists('behavior_plugins[style][settings][groups][italic_test_group]');
    $this->click('#edit-behavior-plugins-style-settings-groups-italic-test-group');
    $this->assertSession()->assertWaitOnAjaxRequest();
    $this->click('#edit-behavior-plugins-style-settings-groups-regular-test-group');
    $this->assertSession()->assertWaitOnAjaxRequest();
    $edit = [
      'behavior_plugins[style][settings][groups_defaults][italic_test_group][default]' => '',
    ];
    $this->submitForm($edit, t('Save'));

    // Check that the style without a style group is no longer available.
    $this->drupalGet('node/add/paragraphed_test');
    $page->pressButton('List additional actions');
    $page->pressButton('paragraphs_test_style_plugin_add_more');
    $this->assertSession()->assertWaitOnAjaxRequest();

    // Since Italic Group defines only two styles, assert that only they appear.
    $options = $this->xpath('//select[contains(@id, :id)]/option', [':id' => 'edit-paragraphs-0-behavior-plugins-style-style']);
    $this->assertCount(3, $options);
    $this->assertEquals('- Default -', $options[0]->getHtml());
    $this->assertEquals('Bold', $options[1]->getHtml());
    $this->assertEquals('Italic', $options[2]->getHtml());

    // Configure Regular as a default style.
    $this->drupalGet('admin/structure/paragraphs_type/' . $paragraph_type);
    $this->assertSession()->fieldValueEquals('behavior_plugins[style][settings][groups_defaults][italic_test_group][default]', '');
    $this->click('#edit-behavior-plugins-style-settings-groups-italic-test-group');
    $this->assertSession()->assertWaitOnAjaxRequest();
    $this->click('#edit-behavior-plugins-style-settings-groups-regular-test-group');
    $this->assertSession()->assertWaitOnAjaxRequest();
    $edit = [
      'behavior_plugins[style][settings][groups_defaults][regular_test_group][default]' => 'regular',
    ];
    $this->submitForm($edit, t('Save'));

    // Regular style should be shown first in the list.
    $this->drupalGet('node/add/paragraphed_test');
    $page->pressButton('List additional actions');
    $page->pressButton('paragraphs_test_style_plugin_add_more');
    $this->assertSession()->assertWaitOnAjaxRequest();
    $page->clickLink('Behavior');
    $option = $this->assertSession()->selectExists('paragraphs[0][behavior_plugins][style][style_wrapper][styles][regular_test_group]')->getValue();
    $this->assertEquals('regular', $option);
    $options = $this->xpath('//select[contains(@id, :id)]/option', [':id' => 'edit-paragraphs-0-behavior-plugins-style-style']);
    $this->assertCount(3, $options);
    $this->assertEquals('- Regular -', $options[0]->getHtml());
    $this->assertEquals('Bold', $options[1]->getHtml());

    // Default style should be applied.
    $page->clickLink('Content');
    $page->fillField('title[0][value]', 'style_plugin_node');
    $page->fillField('paragraphs[0][subform][paragraphs_text][0][value]', 'I am regular text.');
    $this->submitForm([], 'Save');

    // Assert the theme suggestion added by the style plugin.
    $this->assertSession()->pageTextContains('paragraph__test_style_plugin__regular');

    $style = $this->xpath('//div[@class="regular regular-wrapper paragraphs-behavior-style--regular paragraph paragraph--type--test-style-plugin paragraph--view-mode--default"]')[0];
    $this->assertNotNull($style);

    // Assert default value for the style selection.
    $node = $this->getNodeByTitle('style_plugin_node');
    $this->drupalGet('node/' . $node->id() . '/edit');
    $option = $this->assertSession()->selectExists('paragraphs[0][behavior_plugins][style][style_wrapper][styles][regular_test_group]')->getValue();
    $this->assertEquals('regular', $option);

    // Update the styles group configuration.
    $this->drupalGet('admin/structure/paragraphs_type/' . $paragraph_type);
    $this->click('#edit-behavior-plugins-style-settings-groups-bold-test-group');
    $this->assertSession()->assertWaitOnAjaxRequest();
    $this->click('#edit-behavior-plugins-style-settings-groups-overline-test-group');
    $this->assertSession()->assertWaitOnAjaxRequest();
    $this->click('#edit-behavior-plugins-style-settings-groups-empty-test-group');
    $this->assertSession()->assertWaitOnAjaxRequest();
    $edit = [
      'behavior_plugins[style][settings][groups_defaults][bold_test_group][default]' => 'bold',
    ];
    $this->submitForm($edit, 'Save');

    // Assert the values on the behavior form.
    $this->drupalGet($node->toUrl('edit-form'));
    $this->assertSession()->pageTextNotContains('Bold CONTEXT');
    $this->assertSession()->pageTextNotContains('Empty Test Group');

    // Regular and Overline style groups are visible.
    $this->assertSession()->optionExists('edit-paragraphs-0-behavior-plugins-style-style-wrapper-styles-regular-test-group', 'regular');
    $this->assertSession()->optionExists('edit-paragraphs-0-behavior-plugins-style-style-wrapper-styles-overline-test-group', '');
    // Bold and Empty style groups are not visible as they have exactly one
    // item in the list.
    $this->assertSession()->responseNotContains('edit-paragraphs-0-behavior-plugins-style-style-wrapper-styles-bold-test-group');
    $this->assertSession()->responseNotContains('edit-paragraphs-0-behavior-plugins-style-style-wrapper-styles-empty-test-group');
    $this->submitForm([], 'Save');

    // Regular style has been selected through the form.
    $this->assertCount(1, $this->cssSelect('.paragraphs-behavior-style--regular'));
    // Default Bold style has been applied in the background.
    $this->assertCount(1, $this->cssSelect('.paragraphs-behavior-style--bold'));
    // Overline style has not been applied as it has no default option.
    $this->assertSession()->elementNotExists('css','.paragraphs-behavior-style--overline');
    // Empty style has not been applied as it has no default option nor styles.
    $this->assertSession()->elementNotExists('css','.paragraphs-behavior-style--empty');

    $this->drupalGet('admin/structure/paragraphs_type/' . $paragraph_type);
    $edit = [
      // Set default style for the overline group.
      'behavior_plugins[style][settings][groups_defaults][overline_test_group][default]' => 'overline',
      // Remove default style for the bold group.
      'behavior_plugins[style][settings][groups_defaults][bold_test_group][default]' => '',
    ];
    $this->submitForm($edit, 'Save');
    $this->drupalGet('admin/reports/paragraphs_collection/styles');
    $edit = [
      'styles[bold][enabled]' => TRUE,
      'styles[italic][enabled]' => TRUE,
      'styles[underline][enabled]' => TRUE,
      'styles[overline][enabled]' => TRUE,
      // Disable regular style.
      'styles[regular][enabled]' => FALSE,
    ];
    $this->submitForm($edit, 'Save configuration');

    $this->drupalGet($node->toUrl());
    // The new default overline style applies to the previously saved paragraph.
    $this->assertCount(1, $this->cssSelect('.paragraphs-behavior-style--overline'));
    // The bold style has no default and no longer applies.
    $this->assertSession()->elementNotExists('css','.paragraphs-behavior-style--bold');
    // The regular style is disabled and no longer applies.
    $this->assertSession()->elementNotExists('css','.paragraphs-behavior-style--regular');

    // Default overline style is selected and all overline styles are disabled.
    // The empty form element should not be displayed.
    $this->drupalGet('admin/reports/paragraphs_collection/styles');
    $edit = [
      'styles[bold][enabled]' => FALSE,
      'styles[italic][enabled]' => TRUE,
      'styles[underline][enabled]' => TRUE,
      // Disable overline style.
      'styles[overline][enabled]' => FALSE,
      'styles[regular][enabled]' => FALSE,
    ];
    $this->submitForm($edit, 'Save configuration');
    $this->drupalGet($node->toUrl('edit-form'));
    $this->assertSession()->fieldNotExists('paragraphs[0][behavior_plugins][style][style_wrapper][styles][overline_test_group]');
    $this->assertSession()->responseNotContains('edit-paragraphs-0-behavior-plugins-style');
  }

  /**
   * Tests style settings summary.
   */
  public function testStyleSettingsSummary() {
    // Install Paragraph Collection Test in order to have styles.
    \Drupal::service('module_installer')->install(['paragraphs_collection_test']);

    $this->addParagraphedContentType('paragraphed_test');
    $this->loginAsAdmin([
      'create paragraphed_test content',
      'edit any paragraphed_test content',
      'edit behavior plugin settings',
    ]);

    // Create text paragraph.
    $text_paragraph = Paragraph::create([
      'type' => 'text',
      'paragraphs_text' => [
        'value' => '<p>Test text 1.</p>',
        'format' => 'basic_html',
      ],
    ]);
    $text_paragraph->save();

    // Create a container paragraph for the text paragraph.
    $paragraph = Paragraph::create([
      'title' => 'Demo Paragraph',
      'type' => 'container',
      'paragraphs_container_paragraphs' => [$text_paragraph],
    ]);
    $paragraph->save();

    // Create a node with the paragraphs content.
    $node = Node::create([
      'title' => 'Style plugin test',
      'type' => 'paragraphed_test',
      'field_paragraphs' => [$paragraph],
    ]);
    $node->save();

    // Check the empty summary.
    $behavior_plugins = $paragraph->getParagraphType()->get('behavior_plugins');
    $behavior_plugins['style'] = [
      'enabled' => TRUE,
      'groups' => ['bold_test_group' => ['default' => '']],
    ];
    $paragraph->getParagraphType()->set('behavior_plugins', $behavior_plugins);
    $paragraph->getParagraphType()->save();
    $style_plugin = $paragraph->getParagraphType()->getEnabledBehaviorPlugins()['style'];
    $this->assertEquals([], $style_plugin->settingsSummary($paragraph));

    // Use bold style for this container.
    $paragraph->setBehaviorSettings('style', ['styles' => ['bold_test_group' => 'bold']]);
    $paragraph->save();
    $this->assertEquals([['label' => 'Bold CONTEXT', 'value' => 'Bold']], $style_plugin->settingsSummary($paragraph));

    $this->drupalGet('admin/structure/paragraphs_type/' . $paragraph->getParagraphType()->id());
    $this->assertSession()->pageTextContains('Bold Test Group');

    // Check the settings summary in a closed mode.
    $this->drupalGet('admin/structure/types/manage/paragraphed_test/form-display');
    $this->submitForm([], 'field_paragraphs_settings_edit');
    $this->assertSession()->assertWaitOnAjaxRequest();
    $this->submitForm(['fields[field_paragraphs][settings_edit_form][settings][edit_mode]' => 'closed'], t('Update'));
    $this->submitForm([], 'Save');
    $this->drupalGet('node/' . $node->id() . '/edit');
    $this->assertSession()->responseContains('edit-field-paragraphs-0-top-icons');
    $this->assertSession()->responseContains('<span class="summary-content">Test text 1.</span></div><div class="paragraphs-plugin-wrapper"><span class="summary-plugin"><span class="summary-plugin-label">Bold CONTEXT</span>Bold');

    // Configure style bold as default.
    $this->drupalGet('admin/structure/paragraphs_type/' . $paragraph->getType());
    $edit = [
      'behavior_plugins[style][settings][groups_defaults][bold_test_group][default]' => 'bold',
    ];
    $this->submitForm($edit, t('Save'));

    // Check that the settings summary does not show the default style.
    $this->drupalGet('node/' . $node->id() . '/edit');
    $this->assertSession()->responseContains('<span class="summary-content">Test text 1.');
    $this->assertSession()->responseNotContains('Style: Bold');
    $this->assertSession()->responseNotContains('Style: - Bold -');
  }

  /**
   * Tests style plugin with no styles available.
   */
  public function testNoStylesAvailable() {
    $this->addParagraphedContentType('paragraphed_test', 'paragraphs');
    $this->loginAsAdmin([
      'edit any paragraphed_test content',
      'edit behavior plugin settings',
    ]);
    $this->drupalGet('admin/structure/paragraphs_type/add');

    // Create Paragraph type with Style plugin enabled.
    $paragraph_type = 'test_style_plugin';
    $this->addParagraphsType($paragraph_type);
    // Add a text field.
    $this->fieldUIAddExistingField('admin/structure/paragraphs_type/' . $paragraph_type, 'paragraphs_text', 'paragraphs_text');
    $this->drupalGet('admin/structure/paragraphs_type/' . $paragraph_type);
    $options = $this->xpath('//*[contains(@id,"edit-behavior-plugins-style-settings-groups")]/option');
    $this->assertCount(0, $options);
    $edit = [
      'behavior_plugins[style][enabled]' => TRUE,
    ];
    $this->submitForm($edit, t('Save'));
    // Make sure there is an error message shown for the style group.
    $this->assertSession()->pageTextContains('There is no style group available, the style plugin can not be enabled.');
  }

  /**
   * Tests global settings for style plugin.
   */
  public function testGlobalStyleSettings() {
    // Install paragraphs collection test to use test style plugins.
    \Drupal::service('module_installer')->install(['paragraphs_collection_test']);
    $this->addParagraphedContentType('paragraphed_test', 'paragraphs');
    $this->loginAsAdmin([
      'edit any paragraphed_test content',
      'edit behavior plugin settings',
    ]);

    // Create Paragraph type with Style plugin enabled.
    $paragraph_type = 'test_style_plugin';
    $this->addParagraphsType($paragraph_type);
    // Add a text field.
    $this->fieldUIAddExistingField('admin/structure/paragraphs_type/' . $paragraph_type, 'paragraphs_text', 'paragraphs_text');
    $this->drupalGet('admin/structure/paragraphs_type/' . $paragraph_type);
    $this->click('#edit-behavior-plugins-style-enabled');
    $this->click('#edit-behavior-plugins-style-settings-groups-bold-test-group');
    $this->assertSession()->assertWaitOnAjaxRequest();
    $this->click('#edit-behavior-plugins-style-settings-groups-regular-test-group');
    $this->assertSession()->assertWaitOnAjaxRequest();

    $this->submitForm([], t('Save'));
    $this->drupalGet('admin/structure/paragraphs_type/' . $paragraph_type);

    // Assert global settings.
    $this->drupalGet('admin/reports/paragraphs_collection/styles');
    $this->assertSession()->checkboxNotChecked('styles[bold][enabled]');
    $this->assertSession()->checkboxNotChecked('styles[italic][enabled]');
    $this->assertSession()->checkboxNotChecked('styles[regular][enabled]');
    $this->assertSession()->checkboxNotChecked('styles[underline][enabled]');

    // Add a node with paragraphs and check the available styles.
    $this->drupalGet('node/add/paragraphed_test');
    $page = $this->getSession()->getPage();
    $page->pressButton('List additional actions');
    $page->pressButton('paragraphs_' . $paragraph_type . '_add_more');
    $this->assertSession()->assertWaitOnAjaxRequest();
    $page->clickLink('Behavior');
    $options = $this->xpath('//*[contains(@class,"paragraphs-plugin-form-element")]/option');
    $this->assertCount(6, $options);
    $page->fillField('title[0][value]', 'global_settings');
    $edit = [
      'paragraphs[0][behavior_plugins][style][style_wrapper][styles][bold_test_group]' => 'bold'
    ];
    $this->submitForm($edit, 'Save');
    $this->assertSession()->responseContains('paragraphs-behavior-style--bold');
    $this->drupalGet('admin/structure/paragraphs_type/' . $paragraph_type);
    $this->click('#edit-behavior-plugins-style-settings-groups-italic-test-group');
    $this->assertSession()->assertWaitOnAjaxRequest();
    $this->click('#edit-behavior-plugins-style-settings-groups-bold-test-group');
    $this->assertSession()->assertWaitOnAjaxRequest();
    $this->submitForm([], t('Save'));

    // Update global settings and enable two styles.
    $this->drupalGet('admin/reports/paragraphs_collection/styles');
    $this->click('#edit-styles-italic-enabled');
    $this->submitForm([], 'Save configuration');
    $node = $this->getNodeByTitle('global_settings');
    $this->drupalGet('node/' . $node->id());
    // Assert that the class of the plugin is not added if disabled.
    $this->assertSession()->responseNotContains('paragraphs-behavior-style--bold');
    $this->clickLink('Edit');
    // Assert that only the two enabled styles are available.
    $options = $this->xpath('//*[contains(@class,"paragraphs-plugin-form-element")]/option');
    $this->assertCount(2, $options);
    $this->assertEquals($options[0]->getHtml(), '- Default -');
    $this->assertEquals($options[1]->getHtml(), 'Italic');

    $this->drupalGet('admin/structure/paragraphs_type/' . $paragraph_type);
    $options = $this->xpath('//*[contains(@name,"behavior_plugins[style][settings][groups_defaults][italic_test_group][default]")]/option');
    $this->assertCount(2, $options);
    $this->assertEquals($options[0]->getHtml(), '- None -');
    $this->assertEquals($options[1]->getHtml(), 'Italic');

    // Enable bold and italic styles.
    $this->drupalGet('admin/reports/paragraphs_collection/styles');
    $edit = [
      'styles[bold][enabled]' => TRUE,
      'styles[italic][enabled]' => TRUE,
    ];
    $this->submitForm($edit, 'Save configuration');
    // Set default style to italic.
    $this->drupalGet('admin/structure/paragraphs_type/' . $paragraph_type);
    $this->click('#edit-behavior-plugins-style-settings-groups-bold-test-group');
    $this->assertSession()->assertWaitOnAjaxRequest();
    $edit = [
      'behavior_plugins[style][settings][groups_defaults][italic_test_group][default]' => 'italic',
    ];
    $this->submitForm($edit, t('Save'));
    // Set the paragraph style to bold.
    $this->drupalGet('node/' . $node->id() . '/edit');
    $page->clickLink('Behavior');
    $this->submitForm(['paragraphs[0][behavior_plugins][style][style_wrapper][styles][italic_test_group]' => 'bold'], t('Save'));
    $this->assertSession()->responseContains('paragraphs-behavior-style--bold');
    // Assert that the selection is correctly displayed.
    $this->drupalGet('node/' . $node->id() . '/edit');
    $this->assertSession()->optionExists('edit-paragraphs-0-behavior-plugins-style-style-wrapper-styles-italic-test-group', 'bold');

    // Disable the bold style.
    $this->drupalGet('admin/reports/paragraphs_collection/styles');
    $edit = [
      'styles[bold][enabled]' => FALSE,
    ];
    $this->submitForm($edit, 'Save configuration');
    // The plugin should fallback on the default style defined.
    $this->drupalGet('node/' . $node->id());
    $this->assertSession()->responseContains('paragraphs-behavior-style--italic');
  }

  /**
   * Tests the multiple style selection plugin settings and functionality.
   */
  public function testMultipleGroups() {
    // Install Paragraph Collection Test in order to have styles.
    \Drupal::service('module_installer')
      ->install(['paragraphs_collection_test']);

    $this->addParagraphedContentType('paragraphed_test', 'paragraphs');
    $this->loginAsAdmin([
      'edit any paragraphed_test content',
      'edit behavior plugin settings',
    ]);
    $this->drupalGet('admin/structure/paragraphs_type/add');

    // Create Paragraph type with Style plugin enabled.
    $paragraph_type = 'test_style_plugin';
    $this->addParagraphsType($paragraph_type);
    // Add a text field.
    $this->fieldUIAddExistingField('admin/structure/paragraphs_type/' . $paragraph_type, 'paragraphs_text', $paragraph_type);

    // Restrict the paragraphs type to the "Italic Test Group" style group.
    $this->drupalGet('admin/structure/paragraphs_type/' . $paragraph_type);
    $this->click('#edit-behavior-plugins-style-enabled');
    $this->click('#edit-behavior-plugins-style-settings-groups-italic-test-group');
    $this->assertSession()->assertWaitOnAjaxRequest();
    $edit = [
      'behavior_plugins[style][settings][groups_defaults][italic_test_group][default]' => 'italic',
    ];
    $this->submitForm($edit, t('Save'));
    // Create a paragraphed test node and check the style classes.
    $this->drupalGet('node/add/paragraphed_test');
    $page = $this->getSession()->getPage();
    $page->pressButton('List additional actions');
    $page->pressButton('paragraphs_test_style_plugin_add_more');
    $this->assertSession()->assertWaitOnAjaxRequest();
    // Since Italic Group defines only two styles, assert that only they appear.
    $page->clickLink('Behavior');
    $options = $this->xpath('//select[contains(@id, :id)]/option', [':id' => 'edit-paragraphs-0-behavior-plugins-style-style']);
    $this->assertCount(2, $options);
    $this->assertEquals('- Italic -', $options[0]->getHtml());
    $this->assertEquals('Bold', $options[1]->getHtml());
    $page->clickLink('Content');
    $edit = [
      'title[0][value]' => 'title_to_remember',
      'paragraphs[0][subform][paragraphs_text][0][value]' => 'text to apply styles'
    ];
    $this->submitForm($edit, 'Save');
    $this->assertSession()->responseContains('paragraphs-behavior-style--italic');

    // Configure two groups and set their defaults.
    $this->drupalGet('admin/structure/paragraphs_type/' . $paragraph_type);
    $this->click('#edit-behavior-plugins-style-settings-groups-regular-test-group');
    $this->assertSession()->assertWaitOnAjaxRequest();
    $edit = [
      'behavior_plugins[style][settings][groups_defaults][regular_test_group][default]' => 'regular',
      'behavior_plugins[style][settings][groups_defaults][italic_test_group][default]' => 'italic',
    ];
    $this->submitForm($edit, t('Save'));
    // Check the selects elements for each enabled group and check the classes.
    $node = $this->getNodeByTitle('title_to_remember');
    $this->drupalGet('node/' . $node->id() . '/edit');
    $options = $this->xpath('//select[contains(@name, :name)]/option', [':name' => 'paragraphs[0][behavior_plugins][style][style_wrapper][styles][regular_test_group]']);
    $this->assertCount(3, $options);
    $this->assertEquals('- Regular -', $options[0]->getHtml());
    $this->assertEquals('Bold', $options[1]->getHtml());
    $options = $this->xpath('//select[contains(@name, :name)]/option', [':name' => 'paragraphs[0][behavior_plugins][style][style_wrapper][styles][italic_test_group]']);
    $this->assertCount(2, $options);
    $this->assertEquals('- Italic -', $options[0]->getHtml());
    $this->assertEquals('Bold', $options[1]->getHtml());
    $this->drupalGet('node/' . $node->id());
    $this->assertSession()->responseContains('paragraphs-behavior-style--italic');
    $this->assertSession()->responseContains('paragraphs-behavior-style--regular');

    // Configure Regular as a default style.
    $this->drupalGet('admin/structure/paragraphs_type/' . $paragraph_type);
    $this->click('#edit-behavior-plugins-style-settings-groups-italic-test-group');
    $this->assertSession()->assertWaitOnAjaxRequest();
    $edit = [
      'behavior_plugins[style][settings][groups_defaults][regular_test_group][default]' => 'bold',
    ];
    $this->submitForm($edit, t('Save'));
    // Check that there is only one select and only one style class.
    $this->drupalGet('node/' . $node->id() . '/edit');
    $options = $this->xpath('//select[contains(@name, :name)]/option', [':name' => 'paragraphs[0][behavior_plugins][style][style_wrapper][styles][regular_test_group]']);
    $this->assertCount(3, $options);
    $this->assertEquals('- Bold -', $options[0]->getHtml());
    $this->assertEquals('Overridden style Module', $options[1]->getHtml());
    $this->assertEquals('Regular', $options[2]->getHtml());
    $styles = $this->xpath('//select[contains(@name, :name)]', [':name' => 'paragraphs[0][behavior_plugins][style][style_wrapper][styles][italic_test_group]']);
    $this->assertEquals([], $styles);
    $this->drupalGet('node/' . $node->id());
    $this->assertSession()->responseNotContains('paragraphs-behavior-style--italic');
    $this->assertSession()->responseContains('paragraphs-behavior-style--bold');

    // Configure Regular as a default style.
    $this->drupalGet('admin/structure/paragraphs_type/' . $paragraph_type);
    $this->click('#edit-behavior-plugins-style-settings-groups-italic-test-group');
    $this->assertSession()->assertWaitOnAjaxRequest();
    $this->click('#edit-behavior-plugins-style-settings-groups-underline-test-group');
    $this->assertSession()->assertWaitOnAjaxRequest();
    $edit = [
      'behavior_plugins[style][settings][groups_defaults][italic_test_group][default]' => 'italic',
      'behavior_plugins[style][settings][groups_defaults][regular_test_group][default]' => 'regular',
      'behavior_plugins[style][settings][groups_defaults][underline_test_group][default]' => 'underline',
    ];
    $this->submitForm($edit, t('Save'));
    // Check that there is only one select and only one style class.
    $this->drupalGet('node/' . $node->id() . '/edit');
    $options = $this->xpath('//select[contains(@name, :name)]/option', [':name' => 'paragraphs[0][behavior_plugins][style][style_wrapper][styles][regular_test_group]']);
    $this->assertCount(3, $options);
    $this->assertEquals('- Regular -', $options[0]->getHtml());
    $this->assertEquals('Bold', $options[1]->getHtml());
    $options = $this->xpath('//select[contains(@name, :name)]/option', [':name' => 'paragraphs[0][behavior_plugins][style][style_wrapper][styles][italic_test_group]']);
    $this->assertCount(2, $options);
    $this->assertEquals('- Italic -', $options[0]->getHtml());
    $this->assertEquals('Bold', $options[1]->getHtml());
    $options = $this->xpath('//select[contains(@name, :name)]/option', [':name' => 'paragraphs[0][behavior_plugins][style][style_wrapper][styles][underline_test_group]']);
    $this->assertCount(2, $options);
    $this->assertEquals('- Underline -', $options[0]->getHtml());
    $this->assertEquals('Bold', $options[1]->getHtml());
    $this->drupalGet('node/' . $node->id());
    $this->assertSession()->responseContains('paragraphs-behavior-style--italic');
    $this->assertSession()->responseContains('paragraphs-behavior-style--regular');
    $this->assertSession()->responseContains('paragraphs-behavior-style--underline');

    // Change a plugin.
    $this->drupalGet('node/' . $node->id() . '/edit');
    $page->clickLink('Behavior');
    $edit = [
      'paragraphs[0][behavior_plugins][style][style_wrapper][styles][regular_test_group]' => 'bold'
    ];
    $this->submitForm($edit, 'Save');
    $this->assertSession()->responseContains('paragraphs-behavior-style--italic');
    $this->assertSession()->responseContains('paragraphs-behavior-style--bold');
    $this->assertSession()->responseContains('paragraphs-behavior-style--underline');
    // Assert the theme suggestion added by the style plugin.
    $this->assertSession()->pageTextContains('paragraph__test_style_plugin__bold');
    $this->assertSession()->pageTextContains('paragraph__test_style_plugin__italic');
  }

  /**
   * Tests the style overriding with sub themes.
   */
  public function testStyleOverriding() {
    // Install theme c and assert that the gotten style has the class "c".
    \Drupal::service('module_installer')->install(['paragraphs_collection_test']);

    $style_discovery = \Drupal::getContainer()->get('paragraphs_collection.style_discovery');
    $style = $style_discovery->getStyle('style-overridden');
    $this->assertEquals($style['title'], new TranslatableMarkup('Overridden style Module'));
    $this->assertEquals($style['classes'], ['overridden-style-module']);

    \Drupal::service('theme_installer')->install(['paragraphs_collection_test_theme_a']);
    $style = $style_discovery->getStyle('style-overridden');
    $this->assertEquals($style['title'], new TranslatableMarkup('Overridden style A'));
    $this->assertEquals($style['classes'], ['overridden-style-a']);

    \Drupal::service('theme_installer')->uninstall(['paragraphs_collection_test_theme_a']);
    $style = $style_discovery->getStyle('style-overridden');
    $this->assertEquals($style['title'], new TranslatableMarkup('Overridden style C'));
    $this->assertEquals($style['classes'], ['overridden-style-c']);
  }

  /**
   * Tests the style template picking.
   */
  public function testStyleTemplate() {
    // Install paragraphs collection test to use test style plugins.
    \Drupal::service('module_installer')->install(['paragraphs_collection_test']);
    \Drupal::service('theme_installer')->install(['paragraphs_collection_test_theme_a']);
    $theme_config = \Drupal::configFactory()->getEditable('system.theme');
    $theme_config->set('default', 'paragraphs_collection_test_theme_a');
    $theme_config->save();
    $this->addParagraphedContentType('paragraphed_test', 'paragraphs');
    $this->loginAsAdmin([
      'edit any paragraphed_test content',
      'edit behavior plugin settings',
    ]);
    // Enable the style plugin.
    $this->drupalGet('admin/structure/paragraphs_type/separator');
    $this->click('#edit-behavior-plugins-style-enabled');
    $this->click('#edit-behavior-plugins-style-settings-groups-regular-test-group');
    $this->assertSession()->assertWaitOnAjaxRequest();
    $this->submitForm([], t('Save'));
    // Add a Separator paragraph and check if it uses the paragraph type
    // template.
    $this->drupalGet('node/add/paragraphed_test');
    $page = $this->getSession()->getPage();
    $page->pressButton('List additional actions');
    $page->pressButton('paragraphs_separator_add_more');
    $this->assertSession()->assertWaitOnAjaxRequest();
    $edit = [
      'title[0][value]' => 'test_title',
    ];
    $this->submitForm($edit, 'Save');
    // Assert that the Paragraph type template is used.
    $this->assertSession()->pageTextContainsOnce('paragraph-type-template');
    $this->assertSession()->pageTextNotContains('paragraph-style-template');
    // Set the style for the paragraphs and check if it uses the style template.
    $node = $this->getNodeByTitle('test_title');
    $this->drupalGet('node/' . $node->id() . '/edit');
    $page->clickLink('Behavior');
    $edit = [
      'paragraphs[0][behavior_plugins][style][style_wrapper][styles][regular_test_group]' => 'style-overridden',
    ];
    $this->submitForm($edit, 'Save');
    // Assert that the Style template is used.
    $this->assertSession()->pageTextContainsOnce('paragraph-style-template');
    $this->assertSession()->pageTextNotContains('paragraph-type-template');
  }

  /**
   * Adds an existing field through the Field UI.
   *
   * @param string $bundle_path
   *   Admin path of the bundle that the field is to be attached to.
   * @param string $existing_storage_name
   *   The name of the existing field storage for which we want to add a new
   *   field.
   * @param string $label
   *   (optional) The label of the new field. Defaults to a random string.
   * @param array $field_edit
   *   (optional) $edit parameter for submitForm() on the second step
   *   ('Field settings' form).
   */
  public function fieldUIAddExistingField($bundle_path, $existing_storage_name, $label = NULL, array $field_edit = []) {
    $label = $label ?: $this->randomMachineName();
    $field_edit['edit-label'] = $label;

    // First step: navigate to the re-use field page.
    $this->drupalGet("{$bundle_path}/fields/");
    // Confirm that the local action is visible.
    $this->assertSession()->linkExists('Re-use an existing field');
    $this->clickLink('Re-use an existing field');
    $this->assertSession()->assertWaitOnAjaxRequest();
    $this->assertSession()->elementExists('css', "input[value=Re-use][name=$existing_storage_name]");
    $this->click("input[value=Re-use][name=$existing_storage_name]");

    // Set the main content to only the content region because the label can
    // contain HTML which will be auto-escaped by Twig.
    $this->assertSession()->responseContains('field-config-edit-form');
    // Check that the page does not have double escaped HTML tags.
    $this->assertSession()->responseNotContains('&amp;lt;');

    // Second step: 'Field settings' form.
    $this->submitForm($field_edit, 'Save settings');
    $this->assertSession()->pageTextContains("Saved $label configuration.");

    // Check that the field appears in the overview form.
    $xpath = $this->assertSession()->buildXPathQuery("//table[@id=\"field-overview\"]//tr/td[1 and text() = :label]", [
      ':label' => $label,
    ]);
    $this->assertSession()->elementExists('xpath', $xpath);
  }

}
