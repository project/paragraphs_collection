<?php
/**
 * @file
 * Installation hooks for paragraphs collection module.
 */

use Drupal\paragraphs\Entity\ParagraphsType;

/**
 * Add default config for enabled styles.
 */
function paragraphs_collection_update_8001() {
  \Drupal::configFactory()
    ->getEditable('paragraphs_collection.settings')
    ->set('enabled_styles', [])
    ->save();
}

/**
 * Update style plugin config of Paragraph types.
 */
function paragraphs_collection_update_8002() {
  // Clear group and style collection caches.
  \Drupal::cache('discovery')->deleteMultiple(['paragraphs_collection_style', 'paragraphs_collection_style_group']);
  // Get all groups id.
  $groups = \Drupal::service('paragraphs_collection.style_discovery')->getStyleGroupsLabel();
  $group_ids = [];
  foreach ($groups as $group_id => $group) {
    // Use 'label' if exist.
    if (is_array($group)) {
      $group = $group['label'];
    }
    $group_ids[$group->getUntranslatedString()] = $group_id;
  }
  $config_factory = \Drupal::configFactory();
  // Loop over all paragraph types.
  foreach ($config_factory->listAll('paragraphs.paragraphs_type.') as $name) {
    $paragraph_type = $config_factory->getEditable($name);
    if ($paragraph_type->get('behavior_plugins.style')) {
      $default = '';
      // Get the old configuration if the style plugin was enabled.
      $group_label = $paragraph_type->get('behavior_plugins.style.group');
      // Special case for commits before 21bb930.
      $group_combine_ids = array_combine(array_values($group_ids), array_values($group_ids));
      if (isset($group_combine_ids[$group_label])) {
        $group_ids = $group_combine_ids;
      }
      if (!empty($group_label) && isset($group_ids[$group_label])) {
        $default_style = $paragraph_type->get('behavior_plugins.style.default');
        if ($style = \Drupal::service('paragraphs_collection.style_discovery')->getStyle($default_style)) {
          if (in_array($group_ids[$group_label], $style['groups'])) {
            $default = $default_style;
          }
        }
        $paragraph_type->clear('behavior_plugins.style');
        $paragraph_type->set('behavior_plugins.style.enabled', TRUE);
        $paragraph_type->set('behavior_plugins.style.groups.' . $group_ids[$group_label] . '.default', $default);
        $paragraph_type->save();
      }
      elseif (!empty($default_style = $paragraph_type->get('behavior_plugins.style.default'))) {
        // If there is a default style configured, then set the its group as
        // default config.
        if ($style = \Drupal::service('paragraphs_collection.style_discovery')->getStyle($default_style)) {
          if (!empty($style['groups']) && isset($groups[reset($style['groups'])])) {
            $paragraph_type->clear('behavior_plugins.style');
            $paragraph_type->set('behavior_plugins.style.enabled', TRUE);
            $paragraph_type->set('behavior_plugins.style.groups.' . reset($style['groups']) . '.default', $default_style);
            $paragraph_type->save();
          }
        }
      }
    }
  }
}

/**
 * Filter out disabled grid layouts.
 */
function paragraphs_collection_update_8003() {
  $config_factory = \Drupal::configFactory();
  // Loop over all paragraph types.
  foreach ($config_factory->listAll('paragraphs.paragraphs_type.') as $name) {
    $paragraph_type = $config_factory->getEditable($name);
    if ($paragraph_type->get('behavior_plugins.grid_layout')) {
      $available_grid_layouts = $paragraph_type->get('behavior_plugins.grid_layout.available_grid_layouts');
      if (!empty($available_grid_layouts)) {
        // Filter out disabled grid layouts.
        $paragraph_type->set('behavior_plugins.grid_layout.available_grid_layouts', array_filter($available_grid_layouts));
        $paragraph_type->save();
      }
    }
  }
}
