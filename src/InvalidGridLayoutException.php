<?php

namespace Drupal\paragraphs_collection;

/**
 * Exception thrown when a grid layout definition is invalid.
 */
class InvalidGridLayoutException extends \RuntimeException {}
