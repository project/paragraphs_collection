<?php

namespace Drupal\paragraphs_collection;

/**
 * Exception thrown when a style definition is invalid.
 */
class InvalidStyleException extends \RuntimeException {}
