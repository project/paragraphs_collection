<?php

namespace Drupal\paragraphs_collection;

use Drupal\Core\DependencyInjection\AutowireTrait;
use Drupal\Core\DependencyInjection\ContainerInjectionInterface;
use Drupal\Core\StringTranslation\StringTranslationTrait;

/**
 * Defines a class containing permission callbacks.
 */
class Permissions implements ContainerInjectionInterface {

  use StringTranslationTrait;
  use AutowireTrait;

  /**
   * Cosntructor for Permissions class.
   */
  public function __construct(protected StyleDiscoveryInterface $styleDiscovery) {}

  /**
   * Returns an array of permissions for advanced styles.
   *
   * @return array
   */
  public function permissions() {
    $permissions = [];

    // Generate permissions for advanced behavior styles.
    foreach ($this->styleDiscovery->getStyles() as $style) {
      if (isset($style['permission']) && $style['permission'] === TRUE) {
        $permissions['use ' . $style['name'] . ' style'] = [
          'title' => $this->t('Use %style style', ['%style' => $style['title']]),
          'description' => $this->t('Users with this permission can use %style behavior style.', ['%style' => $style['title']]),
        ];
      }
    }

    return $permissions;
  }

}
