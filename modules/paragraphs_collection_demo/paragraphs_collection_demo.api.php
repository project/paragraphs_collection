<?php

/**
 * @file
 * Hooks and documentation related to paragraphs_collection module.
 */

/**
 * @mainpage Paragraphs Collection Demo API documentation.
 *
 * Paragraphs Collection Demo provides multiple paragraph types and various
 * behavior plugins. See paragraphs_collection_demo/README.md for more
 * information.
 *
 * The module provides the following behavior plugins:
 *
 * @section behavior Behavior
 *
 * - @link accordion Accordion @endlink
 * - @link anchor Anchor @endlink
 * - @link background_image Background Image @endlink
 * - @link lockable Lockable @endlink
 * - @link slideshow Slideshow @endlink
 */
