<?php

namespace Drupal\Tests\paragraphs_collection_demo\Functional;

use Drupal\Tests\paragraphs\Functional\WidgetStable\ParagraphsTestBase;

/**
 * Tests Slider plugin.
 *
 * @see \Drupal\paragraphs_collection_demo\Plugin\paragraphs\Behavior\ParagraphsSliderPlugin
 * @group paragraphs_collection_demo
 * @requires module slick
 */
class ParagraphsSliderPluginTest extends ParagraphsTestBase {

  /**
   * Modules to be enabled.
   *
   * @var array
   */
  protected static $modules = [
    'slick',
    'paragraphs_collection',
    'paragraphs_collection_demo',
  ];

  /**
   * {@inheritdoc}
   */
  protected function setUp():void {
    parent::setUp();
    $this->loginAsAdmin([
      'administer modules',
      'edit behavior plugin settings'
    ]);
    $this->addParagraphedContentType('paragraphed_test');
    $this->addParagraphsType('slide_content');
  }

  /**
   * Tests creating slider content.
   */
  public function testCreatingSliderContent() {
    $this->fieldUIAddExistingField('admin/structure/paragraphs_type/slide_content', 'paragraphs_text', 'slide_content');
    $this->assertSession()->pageTextContains('Saved slide_content configuration');

    // Add new content.
    $this->drupalGet('node/add/paragraphed_test');
    $this->submitForm([], 'field_paragraphs_slider_add_more');
    $this->submitForm([], 'field_paragraphs_0_subform_field_slides_slide_content_add_more');
    $this->submitForm([], 'field_paragraphs_0_subform_field_slides_text_add_more');
    $edit = [
      'title[0][value]' => 'Slider',
      'field_paragraphs[0][subform][field_slides][0][subform][paragraphs_text][0][value]' => 'First slide.',
      'field_paragraphs[0][subform][field_slides][1][subform][paragraphs_text][0][value]' => 'Second slide.',
      'field_paragraphs[0][behavior_plugins][slider][slick_slider]' => 'default'
    ];
    $this->submitForm($edit, 'Save');
    $this->assertSession()->pageTextContains('paragraphed_test Slider has been created.');

    $this->drupalGet('node/1');
    $xpath = '//div[@class = "paragraph paragraph--type--slider paragraph--view-mode--default"]//div[contains(@class, "slick")]';
    $this->xpath($xpath, [], "Slick class found");

    $this->drupalGet('admin/structure/paragraphs_type/slider/fields');
    $this->drupalGet('admin/structure/paragraphs_type/slider/fields/paragraph.slider.field_slides/delete');
    $this->submitForm([], t('Delete'));
    $this->assertSession()->pageTextContains('The field Slides has been deleted from the Slider paragraphs type.');

    $node = $this->getNodeByTitle('Slider');
    $this->drupalGet('node/' . $node->id());
    $this->assertSession()->statusCodeEquals(200);

    $this->drupalGet('admin/structure/paragraphs_type/slider');
    $this->assertSession()->statusCodeEquals(200);
    $this->assertSession()->pageTextContains('There are no fields available with the cardinality greater than one. Please add at least one in the Manage fields page.');
    $this->submitForm(['behavior_plugins[slider][enabled]' => TRUE], t('Save'));
    $this->assertSession()->pageTextContains('The Slider plugin cannot be enabled if there is no field to be mapped.');
  }

  /**
   * Tests configuration form for slider plugin.
   */
  public function testConfigurationForm() {

    // Add a new field.
    $this->drupalGet('admin/structure/paragraphs_type/slider/fields/add-field');
    $edit = [
        'new_storage_type' => 'formatted_text',
    ];
    $this->submitForm($edit, 'Continue');
    $edit = [
      'label' => 'Text',
      'field_name' => 'paragraphs_text',
      'group_field_options_wrapper' => 'text_long',
    ];
    $this->submitForm($edit, 'Continue');
    $edit = [
      'field_storage[subform][cardinality]' => '-1',
    ];

    $this->submitForm($edit, 'Save settings');
    $this->assertSession()->pageTextContains('Saved Text configuration');

    //Choose the field to be used as slider items
    $this->drupalGet('admin/structure/paragraphs_type/slider');
    $this->assertSession()->pageTextContains('Slick Optionsets');
    $this->assertSession()->pageTextContains('Enable the Slick UI from the module list to create more options.');
    $edit = [
      'behavior_plugins[slider][settings][field_name]' => 'field_paragraphs_text',
    ];
    $this->submitForm($edit, t('Save'));
    $this->assertSession()->pageTextContains('Saved the Slider Paragraphs type.');

    // Add slider content.
    $this->drupalGet('node/add/paragraphed_test');
    $this->submitForm([], 'field_paragraphs_slider_add_more');
    $this->submitForm([], 'field_paragraphs_text_add_more');
    $edit = [
    'title[0][value]' => 'SldierDemo',
    'field_paragraphs[0][subform][field_paragraphs_text][0][value]' => 'First slide.',
    'field_paragraphs[1][subform][paragraphs_text][0][value]' => 'Second slide',
    'field_paragraphs[0][behavior_plugins][slider][slick_slider]' => 'default',
    ];
    $this->submitForm($edit, 'Save');
    $this->assertSession()->pageTextContains('paragraphed_test SldierDemo has been created.');
  }

  /**
   * Checks getOptionsetDescription method.
   */
  public function testOptionSetDescription() {
    $this->loginAsAdmin();
    $this->drupalGet('admin/structure/paragraphs_type/slider');
    $this->assertSession()->pageTextContains('Select none, to show all.');
    $this->assertSession()->responseNotContains('Enable the Slick UI from the module list to create more options.');
    $this->loginAsAdmin([
      'administer modules',
    ]);
    $this->drupalGet('admin/structure/paragraphs_type/slider');
    $this->assertSession()->pageTextContains('Enable the Slick UI from the module list to create more options.');
    \Drupal::service('module_installer')->install(['slick_ui']);
    $this->loginAsAdmin([
      'administer modules',
      'administer slick',
    ]);
    \Drupal::service('module_installer')->install(['slick_ui']);
    $this->drupalGet('admin/structure/paragraphs_type/slider');
    $this->assertSession()->pageTextContains('To have more options, go to the Slick UI config page and add items there.');
  }

  /**
   * Tests configuration of slider plugin.
   */
  public function testSliderSettingsSummary() {
    $this->loginAsAdmin([
      'create paragraphed_test content',
      'edit any paragraphed_test content',
      'edit behavior plugin settings',
    ]);

    // Add a slide_content paragraph type.
    $this->fieldUIAddExistingField('admin/structure/paragraphs_type/slide_content', 'paragraphs_text', 'slide_content');
    $this->setParagraphsWidgetMode('paragraphed_test', 'field_paragraphs', 'closed');

    // Node edit: add three slides paragraph type.
    $this->drupalGet('node/add/paragraphed_test');
    $this->submitForm([], 'field_paragraphs_slider_add_more');
    $this->submitForm([], 'field_paragraphs_0_subform_field_slides_slide_content_add_more');
    $this->submitForm([], 'field_paragraphs_0_subform_field_slides_text_add_more');
    $edit = [
      'title[0][value]' => 'Slider plugin summary',
      'field_paragraphs[0][subform][field_slides][0][subform][paragraphs_text][0][value]' => 'First slide',
      'field_paragraphs[0][subform][field_slides][1][subform][paragraphs_text][0][value]' => 'Second slide',
      'field_paragraphs[0][behavior_plugins][slider][slick_slider]' => 'default',
    ];
    $this->submitForm($edit, 'Save');
    $this->assertSession()->pageTextContains('paragraphed_test Slider plugin summary has been created.');

    // Assert that the summary includes the text of the behavior plugins.
    $this->clickLink('Edit');
    $this->assertSession()->responseContains('<span class="summary-content">First slide</span>, <span class="summary-content">Second slide</span></div><div class="paragraphs-plugin-wrapper"><span class="summary-plugin"><span class="summary-plugin-label">Slider settings</span>Default');
    $this->xpath('//*[@id="edit-field-paragraphs-0-top-icons"]/span[@class="paragraphs-badge" and @title="2 children"]');
  }

}
